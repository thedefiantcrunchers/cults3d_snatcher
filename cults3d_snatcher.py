import argparse
import mechanize
import re
import os
import urllib.request
import time
import random
from dotenv import load_dotenv
from http.cookiejar import CookieJar

load_dotenv()


def main():
    cj = CookieJar()
    br = mechanize.Browser()
    br.set_cookiejar(cj)
    br.set_handle_robots(False)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

    authenticate(br)

    if args.creations or url.endswith('/creations'):
        process_creations(br, url)
    else:
        fetch_model(br, url)

    print("All Done")
    quit()


def authenticate(br):
    cults3d_user = os.getenv("cults3d_user")
    cults3d_password = os.getenv("cults3d_password")

    print("Authenticating as " + cults3d_user)

    br.open("https://cults3d.com/en/users/sign-in")

    br.select_form(action='/en/users/sign-in')
    br.form['user[email]'] = cults3d_user
    br.form['user[password]'] = cults3d_password
    br.submit()


def process_creations(br, url):
    print("Fetching " + url)
    response = br.open(url).read()

    creations = re.findall(rb'href=\"(.*?)\"><img.*?\n.*?\n.*?\n.*?Free', response)

    for creation in creations:
        fetch_model(br, creation.decode())
        time.sleep(random.randint(1,5))


def fetch_model(br, url):
    print("Fetching " + url)

    slug = re.findall(r'([^\/]+$)',url)[0]
    response = br.open(url).read()

    modeler = re.findall(rb'\"tbox-title drawer-foot\">(.*?)</span>', response)[0]
    modeler = modeler.decode()
    modeler = safe_filename(modeler)

    model = re.findall(rb'\"(.*?)\" property=\"og:title', response)[0]
    model = model.decode()
    model = safe_filename(model)

    br.select_form(action=f'/en/free_orders?creation_slug={slug}')
    br.submit()

    try:
        link = br.find_link(text='Download your files')
        linkUrl = link.url
    except mechanize.LinkNotFoundError:
        print("Download Link not found, looking to use some brute force")
        link = br.find_link(url_regex=r"/en/downloads/\d*")
        linkUrl = re.search(r"(/en/downloads/\d*)", link.url).group(0)

    model_dir = base_dir + os.sep + modeler + os.sep + model
    create_dir(model_dir)

    file = model_dir + os.sep + slug + '.zip'

    if not os.path.exists(file):
        print('Fetching ' + file)
        br.retrieve(f'https://cults3d.com{linkUrl}', file)[0]
    else:
        print(file + ' already exists')


def create_dir(directory):
  if not os.path.exists(directory):
    print("Creating " + directory)
    os.makedirs(directory, exist_ok=True)


def safe_filename(filename):
    filename = re.sub(r'[\\/*?:"<>|]', "_", filename)
    filename = filename.strip().rstrip('.')
    return filename

parser = argparse.ArgumentParser()
parser.add_argument('url', help="URL to parse")
parser.add_argument("--creations", action="store_true", help="URL is a list of creations")
parser.add_argument("-o", "--out", dest="dir", default="cults3d", help="Directory to output files. Defaults to 'cults3d'")

args = parser.parse_args()

base_dir = args.dir
url = args.url

main()
